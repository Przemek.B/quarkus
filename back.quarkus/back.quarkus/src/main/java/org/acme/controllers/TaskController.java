package org.acme.controllers;

import org.acme.entitys.Task;
import org.acme.entitys.TaskDao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@ApplicationScoped
@Path("/tasks")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TaskController {

    @Context
    UriInfo uriInfo;

    @Inject
    TaskDao taskDao;

    @GET
    @Path("{id}")
    public Response getTaskById(@PathParam("id") String id) {
        Task task = taskDao.findById(id);
        if (task == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(task).build();
    }

    @POST
    public Response createTask(Task task) {
        taskDao.create(task);
        return Response.created(uriInfo.getAbsolutePathBuilder().path(task.getId()).build())
                .entity(task)
                .build();
    }

    @DELETE
    @Path("{id}")
    public Response deleteTask(@PathParam("id") String id) {
        taskDao.deleteQueryById(id);
        return Response.ok().build();
    }

    @GET
    public Response getTasks() {
        return Response.ok(taskDao.findAll()).build();
    }

    @PUT
    @Path("{id}")
    public Response updateTask(@PathParam("id") String id, Task task) {
        Task updatedTask = taskDao.findById(id);
        updatedTask.setName(task.getName());
        updatedTask = taskDao.update(updatedTask);
        return Response.ok(updatedTask).build();
    }


}
