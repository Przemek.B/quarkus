package org.acme.controllers;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.internal.common.assertion.Assertion;
import org.acme.entitys.Task;
import org.acme.entitys.TaskDao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;


@QuarkusTest
class TaskControllerTest {

    @Inject
    TaskDao taskDao;

    private String ID = "100";

    private String NAME = "Test Name";

    @Transactional
    @BeforeEach
    void setUpp(){
        Task task = new Task();
        task.setId(ID);
        taskDao.update(task);
    }

    @Test
    void shouldDeleteTask() {

        Task task = taskDao.findById(ID);
        String id = task.getId();
        String location = given()
                .contentType(MediaType.APPLICATION_JSON)
                .delete("/tasks/" + id)
                .prettyPeek()
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract().header(HttpHeaders.LOCATION);

        Assertions.assertNull(location);
    }

    @Test
    void shouldCreateNewTask() {
        Task task = new Task();
        task.setName(NAME);
        String location = given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(task)
                .post("/tasks")
                .prettyPeek()
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode())
                .extract().header(HttpHeaders.LOCATION);

        Task result = given()
                .contentType(MediaType.APPLICATION_JSON)
                .get(location)
                .prettyPeek()
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract().body().as(Task.class);
        Assertions.assertEquals(task.getName(), result.getName());
    }

/*    @Test
    void shouldUpdateTask() {
        Task task = new Task();
        task.setName(NAME);

        Task updatedTask = taskDao.findById(ID);
        String id = updatedTask.getId();
        io.restassured.response.Response response = given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(task)
                .put("/tasks/" + id)
                .prettyPeek()
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract().response();

        Assertions.assertNotNull(response);*/


}