import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Task } from '../models/task';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  tasksUrl = 'http://localhost:8080/tasks';

  constructor( 
    private http: HttpClient) { }

    getTask(id: string): Observable<Task> {
      return this.http.get<Task>(this.tasksUrl + '/' + id);
    }
  
    getTasks(): Observable<Array<Task>> {
      return this.http.get<Array<Task>>(this.tasksUrl);
    }

    addTask(task: Task): Observable<Task> {
      return this.http.post<Task>(this.tasksUrl, task);
    }

  deleteTask(id: string): Observable<void>{
    return this.http.delete<void>(this.tasksUrl + '/' + id);
  }

  updateTask(id: string, task: Task): Observable<Task>{
    return this.http.put<Task>(this.tasksUrl + '/'+id, task)
    
  }
}
