import { TestBed, getTestBed } from '@angular/core/testing';
  import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
  import { NO_ERRORS_SCHEMA } from '@angular/core';
  import { ReactiveFormsModule } from '@angular/forms';
  import { RouterModule } from '@angular/router';
  import { RouterTestingModule } from '@angular/router/testing';

  import { Observable, of } from 'rxjs';
  import { TaskService } from './task.service';
  import { Task } from '../models/task';

describe('TaskService', () => {
  let service: TaskService;
  let injector: TestBed;
  let httpClient: HttpTestingController;

  const URL = 'http://localhost:8080';

  const tasksList = [
    {
      id: '1',
      name: 'Pływanie',
    },
    {
      id: '2',
      name: 'Czytanie',
    },
    {
      id: '3',
      name: 'TestName3',
    }
  ];

const singleTask = {
  id: '2',
  name: 'Czytanie',
};

  beforeEach(() => TestBed.configureTestingModule({
    schemas: [NO_ERRORS_SCHEMA],
    providers: [TaskService],
    imports: [ReactiveFormsModule, RouterModule, HttpClientTestingModule, RouterTestingModule]
  }));

  beforeEach(() => {
    injector = getTestBed();
    httpClient = injector.get(HttpTestingController);
    service = TestBed.inject(TaskService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get find all task', async () => {
    service.getTasks().subscribe(tas => {
      expect(tas).toEqual(tasksList);
    });
    const req = httpClient.expectOne(`${URL}/tasks`);
    req.flush(tasksList);
  });

  it('should add new task', async () => {
    service.addTask(singleTask).subscribe(tas => {
      expect(tas).toEqual(singleTask);
    });
    const req = httpClient.expectOne(`${URL}/tasks`);
    req.flush(singleTask);
  });

  it('should add new task', async () => {
    service.addTask(singleTask).subscribe(tas => {
      expect(tas).toEqual(singleTask);
    });
    const req = httpClient.expectOne(`${URL}/tasks`);
    req.flush(singleTask);
  });

  it('should edit task', () => {
    service.updateTask('2', singleTask).subscribe(data => {
      expect(data).toEqual(singleTask);
    });
    const req = httpClient.expectOne(`${URL}/tasks/2`);
    req.flush(singleTask);
  });

  it('should use GET method when go to the tasks address', () => {
    service.getTasks().subscribe();
    const req = httpClient.expectOne(`${URL}/tasks`);
    expect(req.request.method).toBe('GET');
  });

  it('should use PUT method, to update task', () => {
    service.updateTask('2', singleTask).subscribe();
    const req = httpClient.expectOne(`${URL}/tasks/2`);
    expect(req.request.method).toBe('PUT');
  });

  it('should use DELETE method', () => {
    service.deleteTask('2').subscribe();
    const req = httpClient.expectOne(`${URL}/tasks/2`);
    expect(req.request.method).toBe('DELETE');
  });


});
