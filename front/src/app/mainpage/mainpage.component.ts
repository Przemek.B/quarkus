import { Component, OnInit, OnDestroy } from '@angular/core';
import { Task } from '../models/task';
import { TaskService } from '../services/task.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit,OnDestroy {
  tasks: Array<Task>;
  mainSubscription: Subscription;

  constructor(private taskService: TaskService) { }

  deleteTask(id: string): void {
    this.mainSubscription = this.taskService.deleteTask(id).subscribe();
    location.reload();
  } 
  
  ngOnInit(): void {
    this.mainSubscription = this.taskService.getTasks().subscribe(tas => {
      this.tasks = tas;
    });
  }
  ngOnDestroy(){
    if(this.mainSubscription){
      this.mainSubscription.unsubscribe();
    }
  }

}
