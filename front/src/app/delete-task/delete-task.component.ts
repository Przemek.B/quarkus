import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl,FormBuilder} from '@angular/forms';
import { TaskService } from '../services/task.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-delete-task',
  templateUrl: './delete-task.component.html',
  styleUrls: ['./delete-task.component.css']
})
export class DeleteTaskComponent implements OnInit, OnDestroy {

  id = '';
  deleteSubscription: Subscription;

  deleteTaskForm  = this.fb.group({
    id: ['']
  });

  constructor(private taskService: TaskService,
              private router: Router,
              private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  onSubmit(value): void {
    this.deleteSubscription = this.taskService.deleteTask(value.id).subscribe(task => {
      this.router.navigate(['/quarkus']);
    });
  }
  ngOnDestroy(){
    if(this.deleteSubscription){
      this.deleteSubscription.unsubscribe();
  }
}
}
