import { Component, OnInit } from '@angular/core';
import { Task } from '../models/task';
import { TaskService } from '../services/task.service';
import { FormGroup, FormControl,FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-get-task',
  templateUrl: './get-task.component.html',
  styleUrls: ['./get-task.component.css']
})
export class GetTaskComponent implements OnInit {

  id = '';

  deleteForm  = this.fb.group({
    id: ['']
  });

  constructor(private taskService: TaskService,
    private router: Router,
    private fb: FormBuilder) { }

  ngOnInit(): void { }

  onSubmit(value): void {
    this.router.navigate(['/quarkus/' + value.id]);
  }
}
