import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GetTaskComponent } from './get-task/get-task.component';
import { CreateTaskComponent } from './create-task/create-task.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { NavigationbarComponent } from './navigationbar/navigationbar.component';
import { ReactiveFormsModule, FormsModule, FormBuilder } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DeleteTaskComponent } from './delete-task/delete-task.component';
import { UpdateTaskComponent } from './update-task/update-task.component';
import { PipeCollector } from '@angular/compiler/src/template_parser/binding_parser';

@NgModule({
  declarations: [
    AppComponent,
    GetTaskComponent,
    CreateTaskComponent,
    MainpageComponent,
    NavigationbarComponent,
    DeleteTaskComponent,
    UpdateTaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
