import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { TaskService } from '../services/task.service';
import { Router,ActivatedRoute } from '@angular/router';
import { Task } from '../models/task';

@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.component.html',
  styleUrls: ['./update-task.component.css']
})
export class UpdateTaskComponent implements OnInit {

  id = '';
  name = '';
  task: Task;

  IdForm = this.fb.group({
    id: ['']
  });

  editTaskForm = this.fb.group({
    name: [''],
  });

  constructor(private taskService: TaskService,
    private router: Router, private route: ActivatedRoute, private fb: FormBuilder) { }

    ngOnInit(): void {
      //this.editTaskForm.disable();
      document.getElementById('editButton').setAttribute('disabled', 'disabled');
      this.completeDate();
    }
  
    private completeDate() {
      this.route.params.subscribe(params => {
        if (params.id) {
          this.taskService.getTask(params.id).subscribe(tas => {
            this.id = params.id;
            this.task = tas;
            //document.getElementById('checkId').style.display = 'none';
            this.editTaskForm.enable();
            document.getElementById('editButton').removeAttribute('disabled');
          });
        }
        else {
          document.getElementById('checkId').style.display = 'ok';
        }
      });
    }
  
  onSubmitCheck(value): void {
    this.taskService.getTask(value.id).subscribe(tas => {
      this.id = value.id;
      this.task = tas;
      document.getElementById('editButton').removeAttribute('disabled');
    });
  }

  onSubmit(value): void {
    const task = new Task();
      task.name = this.task.name;
    document.getElementById('editButton').setAttribute('disabled', 'disabled');
    this.taskService.updateTask(this.id, value).subscribe(editetask => {
      this.router.navigate(['/quarkus']);
    });
  }
}
