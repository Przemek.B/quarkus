import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageComponent } from './mainpage/mainpage.component';
import { GetTaskComponent } from './get-task/get-task.component';
import { CreateTaskComponent } from './create-task/create-task.component';
import { DeleteTaskComponent } from './delete-task/delete-task.component';
import { UpdateTaskComponent } from './update-task/update-task.component';


const routes: Routes = [
  { path: 'quarkus', component: MainpageComponent},
  { path: 'quarkus/get', component: GetTaskComponent},
  { path: 'quarkus/add', component: CreateTaskComponent},
  { path: 'quarkus/delete', component: DeleteTaskComponent},
  { path: 'quarkus/update', component: UpdateTaskComponent},
  { path: 'quarkus/update/:id', component: UpdateTaskComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
