import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { TaskService } from '../services/task.service';
import { Router } from '@angular/router'
import { Task } from '../models/task';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css']
})
export class CreateTaskComponent implements OnInit, OnDestroy {

  addSubscription: Subscription;

  taskForm = this.fb.group({
    name: ['']
  });
  
  constructor(private taskService: TaskService,
              private router: Router,
              private fb: FormBuilder) { }

  ngOnInit(): void {}

  onSubmit(value): void {
    const task = new Task();
    task.name = value.name;
    this.taskService.addTask(task).subscribe(() => this.router.navigate(['/quarkus']));
  }
  ngOnDestroy(){
    if(this.addSubscription){
      this.addSubscription.unsubscribe();
    }
  }
}
